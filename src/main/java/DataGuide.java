import controller.DataGuideController;
import controller.InputController;
import controller.IntermediateDataGuideController;
import controller.OutputController;
import model.dataGuide.TXmlDataGuide;
import model.intermediateDataGuide.TmpElement;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DataGuide {

    private static final String[] INPUT_XML = {
            "src/main/resources/catalog1.xml",          // 0
            "src/main/resources/jw_00.xml",             // 1
            "src/main/resources/jw_01.xml",             // 2
            "src/main/resources/jw_02.xml",             // 3
            "src/main/resources/jw_03.xml",             // 4
            "src/main/resources/jw_04.xml",             // 5
            "src/main/resources/jw_05.xml",             // 6
            "src/main/resources/xmlDataGuide.xsd",      // 7
            "src/main/resources/jw_05_double.xml",      // 8
            "src/main/resources/jw_05_freakin_huge.xml" // 9
    };

    private static final String OUTPUT_XML = "src/main/resources/out.xml";

    private static volatile boolean done = false;

    public static void main(final String[] args) throws InterruptedException {
        Thread worker = worker(INPUT_XML[0], OUTPUT_XML);
        Thread progress = progress();
        worker.start();
        progress.start();
        worker.join();
        done = true;
        progress.join();
    }

    private static Thread worker(final String input, final String output) {
        return new Thread(() -> {

            File inputFile = new File(input);
            File outputFile = new File(output);

            InputController inputController = new InputController();
            IntermediateDataGuideController intermediateDataGuideController = new IntermediateDataGuideController();
            DataGuideController dataGuideController = new DataGuideController();
            OutputController outputController = new OutputController();

            Element parsedXml = null;
            try {
                // parse the input xml file.
                parsedXml = inputController.parseXml(inputFile);

            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }

            // simplify the parsed xml file, just keep the relevant structure (node names and nesting).
            TmpElement intermediateDataGuide = intermediateDataGuideController.generateIntermediateDataGuide(parsedXml);
            // System.out.println(intermediateDataGuide);

            // create final data guide in generated xml model.
            TXmlDataGuide dataGuide = dataGuideController.generateDataGuide(intermediateDataGuide);

            try {
                // output xml model to output file.
                outputController.writeToFile(dataGuide, outputFile);

            } catch (IOException | JAXBException e) {
                e.printStackTrace();
            }

        });
    }

    private static Thread progress() {
        return new Thread(() -> {
            System.out.print("running");
            long start = System.currentTimeMillis();
            do {
                System.out.print('.');
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (!done);
            System.out.println("done in " + ((System.currentTimeMillis() - start) / 1000.) + "s");
        });
    }

}

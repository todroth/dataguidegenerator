package controller;

import model.dataGuide.ObjectFactory;
import model.dataGuide.TElement;
import model.dataGuide.TOccurrence;
import model.dataGuide.TXmlDataGuide;
import model.intermediateDataGuide.TmpElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generates the dataGuide based on the intermediate DataGuide.
 */
public class DataGuideController {

    private final ObjectFactory objectFactory = new ObjectFactory();

    /**
     * Every path has to be processed only once.
     * So, if catalog/article/price is processed, is doesn't have to be processed a second time
     * (also if there is a second object that matches the path).
     */
    private List<String> processedPaths;
    private TXmlDataGuide dataGuide;

    public TXmlDataGuide generateDataGuide(final TmpElement rootTmpElement) {
        dataGuide = objectFactory.createTXmlDataGuide();
        processedPaths = new ArrayList<>();

        // save the root element as root in the data guide.
        TElement root = objectFactory.createTElement();
        root.setName(rootTmpElement.getName());

        /* The occurrence of the root element is always ONE,
         * so the following line will always yield ONE,
         * so it can be set directly:
         *
         * root.setOccurrence(getOccurrence(rootTmpElement, rootTmpElement));
         */
        root.setOccurrence(TOccurrence.ONE);

        dataGuide.setElement(root);

        // process the root element's children
        processTmpElementList(rootTmpElement.getChildren(), rootTmpElement, root);

        return dataGuide;
    }

    /**
     * Processes a list of elements.
     *
     * @param tmpElements    The elements to process.
     * @param root           The root element of the whole IntermediateDataGuide structure.
     * @param parentTElement The direct parent element of all the list's elements in the data guide.
     */
    private void processTmpElementList(List<TmpElement> tmpElements, TmpElement root, TElement parentTElement) {

        // iterate over the whole list.
        for (TmpElement tmpElement : tmpElements) {

            // process the element.
            TElement newTElement = processTmpElement(tmpElement, root, parentTElement);

            /* If the element with that specific path has been seen the very first time,
             * the TElement will be created and returned from the processTmpElement-method.
             * In this case, the newTElement is the parent of all child elements inside the data guide.
             *
             * Otherwise, the parent element of the current tmpElement already exists in the data guide,
             * so processTmpElement returns null.
             * In that case, this existing parent element has to be found to be able to process
             * the child elements from the intermediateDataGuide.
             */
            if (newTElement == null) {
                newTElement = findTElement(tmpElement);
            }

            // recursive call: process all the child elements of the current tmpElement.
            processTmpElementList(tmpElement.getChildren(), root, newTElement);
        }
    }

    /**
     * Processes one element.
     *
     * @param tmpElement     The element to process.
     * @param root           The root element of the intermediate data guide.
     * @param parentTElement The direct parent TElement of the element.
     * @return The newly created TElement, or null if that path has already been processed (so the element already exists somewhere in the data guide).
     */
    private TElement processTmpElement(TmpElement tmpElement, TmpElement root, TElement parentTElement) {

        TElement newTElement = null;

        // if a matching element exists in the data guide, processedPaths contains the path and it can be skipped.
        if (!processedPaths.contains(tmpElement.getPathString())) {

            // create a new TElement and set the values.
            newTElement = objectFactory.createTElement();
            newTElement.setName(tmpElement.getName());
            newTElement.setOccurrence(getOccurrence(tmpElement, root));

            // add TElement to the chilren of it's parents.
            parentTElement.getElement().add(newTElement);

            // add the path to the newly created TElement to the processed paths.
            processedPaths.add(tmpElement.getPathString());
        }

        return newTElement;
    }

    /**
     * Finds the TElement in the data guide that matches the specified TmpElement from the intermediate data guide.
     *
     * @param tmpElement The element from the intermediate data guide.
     * @return The TElement, or null if it does not exist.
     */
    private TElement findTElement(TmpElement tmpElement) {

        // if the TmpElement is the root, the corresponding TElement also is the root.
        if (tmpElement.getParent() == null) {
            return dataGuide.getElement();
        }

        // search the tree recursively.
        return findTElementRec(tmpElement, dataGuide.getElement());
    }

    /**
     * Searches the tree for the TmpElement.
     *
     * @param tmpElementToFind The TmpElement to find the corresponding TElement for.
     * @param currentTElement  The current TElement to scan its children.
     * @return The TElement, or null if it does not exist.
     */
    private TElement findTElementRec(TmpElement tmpElementToFind, TElement currentTElement) {

        // if one of the children is the element we're looking for, it can be returned.
        for (TElement child : currentTElement.getElement()) {
            if (child.getName().equals(tmpElementToFind.getName())) {
                return child;
            }
        }

        // otherwise, call function for all of the child nodes.
        for (TElement child : currentTElement.getElement()) {

            // recursive call.
            TElement currentElement = findTElementRec(tmpElementToFind, child);

            // if the recursive call yielded a result != null, that is the node we're looking for.
            if (currentElement != null) {
                return currentElement;
            }
        }

        // node has not been found.
        return null;
    }

    /**
     * (╯°□°)╯︵ ┻━┻
     * <p>
     * The most complex part of this whole program...
     * <p>
     * Every node in the data guide contains information about how often it can be included in the source data.
     * This information can be one of the following values:
     * <p>
     * * ONE
     * * ONE_OR_MANY
     * * ZERO_OR_MANY
     * * ZERO_OR_ONE
     * <p>
     * So, the the original data (simplified by the intermediate data guide structure) has to be scanned for every
     * node of the data guide, and it has to be checked how often the data guide node value is contained in the data.
     *
     * @param element The intermediate data guide TmpElement node to check for.
     * @param root    The root element of the intermediate data guide.
     * @return The TOccurrence value of this element.
     */
    private TOccurrence getOccurrence(TmpElement element, TmpElement root) {

        // if the element does not have a parent, it is the root, so it has the value of ONE.
        if (element.getParent() == null) {
            return TOccurrence.ONE;
        }

        /*
         * The following code can be illustrated with the following pseudo code:
         *
         * if (the path of this object does terminate somewhere in the data) {
         *  min = 0 // there is no object for that path somewhere in the data
         * } else {
         *  min = 1 // following the path does always result in at least one object
         * }
         *
         * if (where the path does not prematurely terminate, a object of this kind is always contained exactly once inside its parent node) {
         *  max = 1 // there is always just one of this node
         * } else {
         *  max = many // there can be many of these nodes
         * }
         *
         */

        // get the path of the TmpElement.
        List<String> path = element.getPath();

        // remove the root of this path
        path.remove(0);

        // add the name of this node as last element into the path
        path.add(element.getName());

        int min;
        if (pathTerminates(path, root)) {
            // if the path terminates somewhere, the minimum value is 0.
            if (checkParents(element, root)) {
                min = 1;
            } else {
                min = 0;
            }
            // min = 0;
        } else {
            min = 1;
        }

        // get the max value (which is ONE or MANY)
        boolean maxOccurrenceIsOne = maxOccurrenceIsOne(element, root);
        TOccurrence occurrence;

        if (maxOccurrenceIsOne) {

            if (min == 0) { // (min: 0, max: 1)
                occurrence = TOccurrence.ZERO_OR_ONE;
            } else { // (min: 1, max: 1)
                occurrence = TOccurrence.ONE;
            }

        } else {

            if (min == 0) { // (min: 0, max: MANY)
                occurrence = TOccurrence.ZERO_OR_MANY;
            } else { // (min: 1, max: MANY)
                occurrence = TOccurrence.ONE_OR_MANY;
            }

        }


        return occurrence;
    }

    private boolean checkParents(TmpElement element, TmpElement root) {
        TmpElement parent = element.getParent();
        List<TmpElement> allAlike = new ArrayList<>();
        // find all elements that belong to the same "class" as the element we're looking for (same path, same name).
        getAllAlike(parent, root, allAlike);
        int numAllAlike = allAlike.size();
        for (TmpElement sameElementAsParent : allAlike) {
            // und schaue ob es die Kind Elemente gibt
            for(TmpElement parentChildElement : sameElementAsParent.getChildren()) {
                if (parentChildElement.getName().equals(element.getName())) {
                    numAllAlike--;
                }
            }
        }
        return numAllAlike == 0;
    }

    /**
     * Checks if the path, starting at the specified root, terminates somewhere, or if it's valid in every case.
     *
     * @param path The path to check for.
     * @param root The root to start the search.
     * @return true if the path terminates somewhere, otherwise false.
     */
    private boolean pathTerminates(List<String> path, TmpElement root) {

        if (path.isEmpty()) {
            throw new IllegalArgumentException("The path must not be empty.");
        }

        /*
         * If there is just one element left in the path, check for the element's children. If at least one of the
         * children is named like the only element of the past, the path does not terminate (false). If there is
         * no object called like the last past element, the path does terminate (true).
         */
        if (path.size() == 1) {
            return root.getChildren().stream()
                    .map(TmpElement::getName)
                    .filter(name -> name.equals(path.get(0)))
                    .count() == 0;
        }

        String nextPathPart = path.get(0);

        // find all child nodes that match the next path element.
        List<TmpElement> matchingElements = root.getChildren().stream()
                .filter(el -> el.getName().equals(nextPathPart))
                .collect(Collectors.toList());

        // if there are no matching child nodes, the path DOES terminate
        if (matchingElements.size() == 0) {

            return true;

        } else {

            // remove first element of the path.
            path.remove(0);

            // recursively check every element one layer further down the tree.
            for (TmpElement child : matchingElements) {
                if (pathTerminates(path, child)) {
                    // the path terminates somewhere, so we can prematurely stop the function and return true.
                    return true;
                }

            }
        }

        /*
         * There was not found any case where the path does terminate,
         * so the deep search always leads to an object, so we can return false (the path DOES NOT terminate).
         */
        return false;
    }

    /**
     * Checks if the maximum occurrence of the TmpElement element is ONE. If not, it must be MANY.
     *
     * @param element The element to check the max occurrence for.
     * @param root    The root element of the intermediate data guide tree.
     * @return true if the max occurrence is one, false if the max occurrence is many.
     */
    private boolean maxOccurrenceIsOne(TmpElement element, TmpElement root) {
        List<TmpElement> allAlike = new ArrayList<>();

        // find all elements that belong to the same "class" as the element we're looking for (same path, same name).
        getAllAlike(element, root, allAlike);

        boolean maxIsOne = true;

        for (TmpElement el : allAlike) {

            /*
             * For every element of the same "class" (same path + same name),
             * check if that kind of element is contained exactly once or multiple times inside its parent element.
             */
            maxIsOne &= el.getParent().getChildren().stream()
                    .map(TmpElement::getName)
                    .filter(currentElement -> currentElement.equals(element.getName()))
                    .count() == 1;
        }

        return maxIsOne;

    }

    /**
     * Finds all elements that are of the same "class" as the specified element.
     * Same "class" == same path + same name.
     * These elements can be seen as "equal" in the data guide.
     *
     * @param element  The element to search for.
     * @param root     The root to start the search.
     * @param allAlike The list of all "alike" elements, that will be filled during the recursive calls.
     */
    private void getAllAlike(TmpElement element, TmpElement root, List<TmpElement> allAlike) {
        for (TmpElement el : root.getChildren()) {

            // if one of the children has the same path and the same name, it can be added to the list.
            if (el.getName().equals(element.getName()) && el.getPathString().equals(element.getPathString())) {
                allAlike.add(el);

                // return from the function if an element was found. further down, there can't be a matching element.
                return;
            }

            // search recursively one layer further down the intermediate data guide tree for the element.
            getAllAlike(element, el, allAlike);
        }
    }

}

package controller;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class InputController {

    /**
     * Parse the xml file into a dom structure.
     *
     * @param xmlFile The xml file to parse.
     * @return The root element of the parsed dom structure.
     */
    public Element parseXml(final File xmlFile) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(xmlFile);
        return document.getDocumentElement();
    }

}

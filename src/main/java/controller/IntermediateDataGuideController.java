package controller;

import model.intermediateDataGuide.TmpElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles the creation of the intermediate temporary data guide structure.
 * It's a simplification of the dom structure that contains only the relevant parts:
 * <p>
 * * The node name
 * * The parent node
 * * the child nodes
 * * The path from the root node
 * <p>
 * With these information, the navigation through the tree upwards and downwards is pretty simple.
 */
public class IntermediateDataGuideController {

    public TmpElement generateIntermediateDataGuide(final Element domRoot) {

        if (domRoot == null) {
            throw new IllegalArgumentException("domRoot must not be null");
        }

        // specify the root element
        TmpElement root = new TmpElement();
        root.setName(domRoot.getNodeName());
        root.setParent(null);

        NodeList childNodes = domRoot.getChildNodes();

        // process all of its children
        List<TmpElement> childElements = processNodeList(childNodes, root);

        // set the children in the root element
        root.setChildren(childElements);

        return root;
    }

    /**
     * Processes a list of (child-) nodes.
     *
     * @param nodeList The nodes to process.
     * @param parent   The direct parent of all of these nodes.
     * @return A list containing the newly created TmpElements.
     */
    private List<TmpElement> processNodeList(final NodeList nodeList, final TmpElement parent) {

        if (nodeList == null) {
            throw new IllegalArgumentException("nodeList must not be null");
        }

        if (parent == null) {
            throw new IllegalArgumentException("parent must not be null");
        }

        List<TmpElement> elements = new ArrayList<>();

        int length = nodeList.getLength();

        // iterate over the nodes.
        for (int i = 0; i < length; ++i) {

            Node node = nodeList.item(i);

            // just process it if it is an element node (and not a xml comment or something like that).
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;

                // process the element.
                TmpElement tmpElement = processElement(element, parent);
                elements.add(tmpElement);
                NodeList childNodes = element.getChildNodes();

                // process all of the element's children.
                List<TmpElement> childElements = processNodeList(childNodes, tmpElement);

                // set the children inside the parent node.
                tmpElement.setChildren(childElements);
            }
        }

        return elements;
    }

    /**
     * Processes one element.
     *
     * @param element The element to process.
     * @param parent  The direct parent of this element.
     * @return
     */
    private TmpElement processElement(final Element element, final TmpElement parent) {

        if (element == null) {
            throw new IllegalArgumentException("element must not be null!");
        }

        if (parent == null) {
            throw new IllegalArgumentException("parent must not be null!");
        }

        // create the element and set the values.
        TmpElement tmpElement = new TmpElement();
        tmpElement.setName(element.getNodeName());
        tmpElement.setParent(parent);

        // tmpElement.path == parentElement.path + parentElement.name
        tmpElement.setPath(parent.getPath());
        tmpElement.addPath(parent.getName());

        // add this element to the list of the parent's children.
        parent.getChildren().add(tmpElement);
        return tmpElement;
    }

}

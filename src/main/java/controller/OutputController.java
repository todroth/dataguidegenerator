package controller;

import model.dataGuide.TXmlDataGuide;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;

public class OutputController {

    /**
     * Write the data guide into an output file.
     *
     * @param dataGuide  The data guide in the generated model.
     * @param outputFile The output file.
     */
    public void writeToFile(final TXmlDataGuide dataGuide, final File outputFile) throws IOException, JAXBException {

        if (!outputFile.exists()) {
            if (!outputFile.getParentFile().exists() && !outputFile.getParentFile().mkdirs()) {
                throw new RuntimeException("Can't create directories");
            }
            if (!outputFile.createNewFile()) {
                throw new RuntimeException("Can't create output file");
            }
        }

        JAXBContext jaxbContext = JAXBContext.newInstance(dataGuide.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, "xmlDataGuide.xsd");

        marshaller.marshal(dataGuide, outputFile);

    }

}

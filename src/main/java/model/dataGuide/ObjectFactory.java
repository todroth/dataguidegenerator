//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.01.02 um 12:19:15 PM CET 
//


package model.dataGuide;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the model package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _XmlDataGuide_QNAME = new QName("", "xmlDataGuide");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: model
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TXmlDataGuide }
     * 
     */
    public TXmlDataGuide createTXmlDataGuide() {
        return new TXmlDataGuide();
    }

    /**
     * Create an instance of {@link TElement }
     * 
     */
    public TElement createTElement() {
        return new TElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TXmlDataGuide }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "xmlDataGuide")
    public JAXBElement<TXmlDataGuide> createXmlDataGuide(TXmlDataGuide value) {
        return new JAXBElement<TXmlDataGuide>(_XmlDataGuide_QNAME, TXmlDataGuide.class, null, value);
    }

}

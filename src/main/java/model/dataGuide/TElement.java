//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.01.02 um 12:19:15 PM CET 
//


package model.dataGuide;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r T_element complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="T_element">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="element" type="{}T_element" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="occurrence" type="{}T_occurrence" default="one" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_element", propOrder = {
    "element"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class TElement {

    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected List<TElement> element;
    @XmlAttribute(name = "name", required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected String name;
    @XmlAttribute(name = "occurrence")
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected TOccurrence occurrence;

    /**
     * Gets the value of the element property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the element property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TElement }
     * 
     * 
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public List<TElement> getElement() {
        if (element == null) {
            element = new ArrayList<TElement>();
        }
        return this.element;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public String getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der occurrence-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TOccurrence }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public TOccurrence getOccurrence() {
        if (occurrence == null) {
            return TOccurrence.ONE;
        } else {
            return occurrence;
        }
    }

    /**
     * Legt den Wert der occurrence-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TOccurrence }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setOccurrence(TOccurrence value) {
        this.occurrence = value;
    }

}

//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.01.02 um 12:19:15 PM CET 
//


package model.dataGuide;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r T_occurrence.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="T_occurrence">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="one"/>
 *     &lt;enumeration value="oneOrMany"/>
 *     &lt;enumeration value="zeroOrMany"/>
 *     &lt;enumeration value="zeroOrOne"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "T_occurrence")
@XmlEnum
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
public enum TOccurrence {

    @XmlEnumValue("one")
    ONE("one"),
    @XmlEnumValue("oneOrMany")
    ONE_OR_MANY("oneOrMany"),
    @XmlEnumValue("zeroOrMany")
    ZERO_OR_MANY("zeroOrMany"),
    @XmlEnumValue("zeroOrOne")
    ZERO_OR_ONE("zeroOrOne");
    private final String value;

    TOccurrence(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TOccurrence fromValue(String v) {
        for (TOccurrence c: TOccurrence.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

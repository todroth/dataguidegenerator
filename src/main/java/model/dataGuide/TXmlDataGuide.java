//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.01.02 um 12:19:15 PM CET 
//


package model.dataGuide;

import javax.annotation.Generated;
import javax.xml.bind.annotation.*;


/**
 * <p>Java-Klasse f�r T_xmlDataGuide complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="T_xmlDataGuide">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="element" type="{}T_element"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlRootElement(name = "xmlDataGuide")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "T_xmlDataGuide", propOrder = {
    "element"
})
@Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
public class TXmlDataGuide {

    @XmlElement(required = true)
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    protected TElement element;

    /**
     * Ruft den Wert der element-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TElement }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public TElement getElement() {
        return element;
    }

    /**
     * Legt den Wert der element-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TElement }
     *     
     */
    @Generated(value = "com.sun.tools.internal.xjc.Driver", date = "2017-01-02T12:19:15+01:00", comments = "JAXB RI v2.2.8-b130911.1802")
    public void setElement(TElement value) {
        this.element = value;
    }

}

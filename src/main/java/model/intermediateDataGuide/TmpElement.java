package model.intermediateDataGuide;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple model for the intermediate data guide.
 * This model has the exact same structure as the raw input data, but:
 * <p>
 * * it is reduced to only the relevant information of the node (the name).
 * * it has additional functionality for navigating through the tree (it knows its children AND its parent).
 */
public class TmpElement {

    private String name;
    private TmpElement parent;
    private List<TmpElement> children;

    /**
     * The path contains the names of all nodes leading to this object.
     * For example, longDescription in catalog1.xml is contained in the following path:
     * <p>
     * [catalog, articles, article]
     */
    private List<String> path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TmpElement getParent() {
        return parent;
    }

    public void setParent(TmpElement value) {
        this.parent = value;
    }

    public List<TmpElement> getChildren() {
        if (children == null) {
            children = new ArrayList<>();
        }
        return new ArrayList<>(children);
    }

    public List<String> getPath() {
        if (path == null) {
            path = new ArrayList<>();
        }
        return new ArrayList<>(path);
    }

    public void setChildren(List<TmpElement> value) {
        children = value;
    }

    public void setPath(List<String> value) {
        path = value;
    }

    public void addPath(String value) {
        if (path == null) {
            path = new ArrayList<>();
        }
        path.add(value);
    }

    public String getPathString() {
        return path.toString() + ", " + getName();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(getPathString());

        sb.append('\n');
        for (TmpElement e : getChildren()) {
            sb.append(e.toString());
        }
        return sb.toString();
    }
}

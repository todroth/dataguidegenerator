package controller;

import model.dataGuide.TElement;
import model.dataGuide.TOccurrence;
import model.dataGuide.TXmlDataGuide;
import model.intermediateDataGuide.TmpElement;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class DataGuideControllerTest {

    private InputController inputController;
    private IntermediateDataGuideController intermediateDataGuideController;
    private DataGuideController dataGuideController;

    @Before
    public void SetUp() {
        inputController = new InputController();
        intermediateDataGuideController = new IntermediateDataGuideController();
        dataGuideController = new DataGuideController();
    }

    @Test
    public void testCatalog1Xml() throws IOException, SAXException, ParserConfigurationException {

        File catalog1 = new File("src/main/resources/catalog1.xml");
        Element parsedXml = inputController.parseXml(catalog1);
        TmpElement intermediateDataGuide = intermediateDataGuideController.generateIntermediateDataGuide(parsedXml);
        TXmlDataGuide dataGuide = dataGuideController.generateDataGuide(intermediateDataGuide);

        assertEquals("catalog", dataGuide.getElement().getName());
        assertEquals(TOccurrence.ONE, dataGuide.getElement().getOccurrence());

        for (TElement catalogEl : dataGuide.getElement().getElement()) {

            assertThat(catalogEl.getName(), anyOf(is("header"), is("articles")));
            assertEquals(TOccurrence.ONE, catalogEl.getOccurrence());

            switch (catalogEl.getName()) {

                case "header":

                    for (TElement headerEl : catalogEl.getElement()) {

                        assertThat(headerEl.getName(), anyOf(is("catalogId"), is("creationDate")));
                        assertEquals(TOccurrence.ONE, headerEl.getOccurrence());

                    }

                    break;

                case "articles":

                    for (TElement articlesEl : catalogEl.getElement()) {

                        assertEquals("article", articlesEl.getName());
                        assertEquals(TOccurrence.ONE_OR_MANY, articlesEl.getOccurrence());

                        for (TElement articleEl : articlesEl.getElement()) {

                            assertThat(articleEl.getName(), anyOf(is("articleId"), is("shortDescription"), is("price"), is("longDescription"), is("deliveryTime")));

                            switch (articleEl.getName()) {

                                case "articleId":
                                case "shortDescription":
                                    assertEquals(TOccurrence.ONE, articleEl.getOccurrence());
                                    break;

                                case "price":
                                    assertEquals(TOccurrence.ONE_OR_MANY, articleEl.getOccurrence());
                                    break;

                                case "longDescription":
                                    assertEquals(TOccurrence.ZERO_OR_MANY, articleEl.getOccurrence());
                                    break;

                                case "deliveryTime":
                                    assertEquals(TOccurrence.ZERO_OR_ONE, articleEl.getOccurrence());
                                    break;

                            }

                        }

                    }

                    break;

            }

        }

    }


}
